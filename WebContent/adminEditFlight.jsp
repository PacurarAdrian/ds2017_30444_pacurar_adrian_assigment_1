<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.HashSet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
DIV.table 
{
    display:table;
   border: 1px solid black;
}
FORM.tr, DIV.tr
{
    display:table-row;
}
SPAN.td,form.td
{
    display:table-cell;
    border: 1px solid black;
}
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=case],input[type=number]{
   
 	width: 60px;	
 	
 	 
}
input[type=text]
{
	border-radius: 4px;
}
input[type=submit]{
	width: 60px;
	
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
</style>
<title>Airport Administrator</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100%>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Airport</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		<form method="GET" action="AdminServlet">
			<input type="submit" name="home" value="Home"/>
		</form>
		<form action="AdminServlet" method="POST">
			<input name="opValue" type="text" placeholder="Flight type or number"/>
			<input type="submit" name="oSearchFlight" value="Search"/>
			<input type="hidden" name="tableName" value="<%=request.getAttribute("tableName")%>">
		 </form>
		 	  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form method="get" action="LogInServlet">Logged in: ${name}  <input type="submit" value="Log out"/></form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<details open>
  	<summary><font size=5>Edit Flight Table</font></summary>
	<fieldset >
	<legend></legend>
	
	
	
	

<div class="table">
	<div class="tr">
		
		<span class="td">Number</span>
    	<span class="td">Type</span>
    	<span class="td">Departure Time</span>
    	<span class="td">Departure City</span>
    	<span class="td">Arrival Time</span>
    	<span class="td">Arrival City</span>
        <span class="td">Arrival City Coordinates</span>
        <span class="td">Departure City CoordinateS</span>
        <span class="td">Operation</span>
    </div>
    <c:forEach var="element" items="${flightData}">
     <c:set var="id" value="${element.id}"></c:set>
        <form class="tr" action="AdminServlet" method="post">
        	
        	<span class="td"><input type="number" value="${element.number}" name="number"></span>
            <span class="td"><input type="case" value="${element.type}" name="type"></span>
            <span class="td"><input type="text" value="${element.departureTime}" name="departureTime"></span> 
            <span class="td">
	            <select name="departureCity" >
		    		<c:forEach var="city" items="${cityData}">				
						<c:choose>
						    <c:when test="${element.departureCity==city.name}">
						        <option value="${city.name}" label="${city.name}" selected/>
						    </c:when>    
						    <c:otherwise>
						        <option value="${city.name}" label="${city.name}"/>     
						    </c:otherwise>
						</c:choose>
					</c:forEach>
	            </select>
            </span>
            <span class="td"><input type="text" value="${element.arrivalTime}" name="arrivalTime"></span>
            <span class="td">
            <select name="arrivalCity" >
	           		
	    		<c:forEach var="city" items="${cityData}">
	    		<c:choose>
				    <c:when test="${element.arrivalCity==city.name}">
				        <option value="${city.name}" label="${city.name}" selected/>
				    </c:when>    
				    <c:otherwise>
				        <option value="${city.name}" label="${city.name}"/>     
				    </c:otherwise>
				</c:choose>
					
				</c:forEach>
            </select>
            
            </span>
            <span class="td">${element.arrivalCityObj}</span>
            <span class="td">${element.departureCityObj}</span>
            
            <span class="td">
            	<input type="hidden" value="${element.id}" name="elemId"> 
                <input type="submit" value="Update" name="oEdit"> 
                <input type="submit" value="Delete" name="oDelete">
            </span> 
        </form>
    </c:forEach>
    
        
    <form  class="tr" action="AdminServlet" method="post">
    
    	<span class="td"><input type="number" size=3 name="number" placeholder="nr"></span>
    	<span class="td"><input type="case" size=4 name="type" placeholder="text"></span>
    	<span class="td"><input type="datetime-local" name="departureTime" placeholder="yyyy-M-dd hh:mm"></span>
    	<span class="td">
    	<select name="departureCity" >
    		<c:forEach var="element" items="${cityData}">
				<option value="${element.name}" label="${element.name}"/>
			</c:forEach>	
		</select>	
    	</span>
    	<span class="td"><input type="datetime-local" name="arrivalTime" placeholder="yyyy-M-dd hh:mm"></span>
    	<span class="td">
    	<select name="arrivalCity" >
    		<c:forEach var="element" items="${cityData}">
				<option value="${element.name}" label="${element.name}"/>
			</c:forEach>	
		</select>
    	</span>
    	 <span class="td"></span>
    	  <span class="td"></span>
    	<span class="td">
    		<input type="submit" name="oInsert" value="Insert">
    		<input type="hidden" name="tableName" value="<%=request.getAttribute("tableName")%>">
    	</span>
    </form>
    
    
</div>
    
	<!-- <form action="AdminServlet" method="post"><table border="1" width="90%">
	<tr>
	<td>id</td>
		<td>Number</td>
    	<td>Type</td>
    	<td>Departure Time</td>
    	<td>Departure City</td>
    	<td>Arrival Time</td>
    	<td>Arrival City</td>
        <td>Arrival City Coordinates</td>
        <td>Departure City Coordinates</td>
        <td>Operation</td>
    </tr>
    <c:forEach var="element" items="${flightData}">
     <c:set var="id" value="${element.id}"></c:set>
        <tr>
        <td>${element.id}</td>
        	<td>${element.number}</td>
            <td>${element.type}</td> 
            <td>${element.departureTime}</td> 
            <td>${element.departureCity}</td>
            <td>${element.arrivalTime}</td>
            <td>${element.arrivalCity}</td>
            <td>${element.arrivalCityObj}</td>
            <td>${element.departureCityObj}</td>
            <td>
            	<form action="AdminServlet" method="get">
                    <input type="text" value="${element.id}" name="elemId"> 
                    <input type="submit" value="Edit" name="oEdit"> 
                    <input type="submit" value="Delete" name="oDelete">
               </form>
            </td>
        </tr> 
    </c:forEach>
    </table>
    
    <form action="AdminServlet" method="post">
    <table border="1" width="90%"> 
    <tr>
   
    	<td><input type="case" name="number" placeholder="Type here new information"></td>
    	<td><input type="case" name="type" placeholder="Type here new information"></td>
    	<td><input type="case" name="departureTime" placeholder="yyyy-M-dd hh:mm:ss"></td>
    	<td>
    	<select name="departureCity" >
    		<c:forEach var="element" items="${cityData}">
				<option value="${element.name}" label="${element.name}"/>
			</c:forEach>	
		</select>	
    	
    	<td><input type="case" name="arrivalTime" placeholder="yyyy-M-dd hh:mm:ss"></td>
    	<td>
    	<select name="arrivalCity" >
    		<c:forEach var="element" items="${cityData}">
				<option value="${element.name}" label="${element.name}"/>
			</c:forEach>	
		</select>
    	</td>
    	<td colspan=3><input type="submit" name="oInsert" value="Insert">
    <input type="hidden" name="tableName" value="<%=request.getAttribute("tableName")%>">
    </td>

    
    </tr>
    
</table>
</form>
	
	
	<input type="submit" name="oApply" value="Apply">
	</form> -->
	<br>
	
	
	</fieldset>
	</details>
	
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
</body>
</html>