<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=text], select{
   
 	
 	border-radius: 4px;
 	 
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
</style>
<title>Airport Administrator</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100%>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Airport</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		<form method="GET" action="AdminServlet">
			<input type="submit" name="home" value="Home"/>
		</form>
		<form action="AdminServlet" method="POST">
			<input name="opValue" type="text" placeholder="Employee name or PNC"/>
			<input type="submit" name="oSearchUser" value="Search"/>
			<input type="hidden" name="tableName" value="<%=request.getAttribute("tableName")%>">
		 </form>
		 	  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form method="get" action="LogInServlet">Logged in: ${name} <input type="submit" value="Log out"/></form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<details open>
  	<summary><font size=5>User table</font></summary>
	<fieldset >
	<legend></legend>
	<form method="GET" action="AdminServlet">
			<input type="submit" name="edit" value="Edit user table">
	</form>
	<br>
	<table border="1" width="90%">
	<tr>
		<td>Name</td>
    	<td>NPC</td>
    	<td>Username</td>
    	<td>Administrator</td>
    </tr>
    <c:forEach var="element" items="${userData}">
    
        <tr>
        
        	<td>${element.name}</td>
            <td>${element.npc}</td> 
            <td>${element.username}</td> 
            <td>${element.admin}</td>
            
        </tr> 
    </c:forEach>
	</table>

	
	<br>
	
	
	</fieldset>
	</details>
	<details open>
  	<summary><font size=5>Flights table</font></summary>
	<fieldset >
	<legend></legend>
	<form method="GET" action="AdminServlet">
			<input type="submit" name="editFlights" value="Edit flights table">
	</form>
	<br>
	<table border="1" width="90%">
	<tr>
		<td>Number</td>
    	<td>Type</td>
    	<td>DepartureTime</td>
    	<td>departureCity</td>
    	<td>ArrivalTime</td>
    	<td>arrivalCity</td>
       
    </tr>
    <c:forEach var="element" items="${flightData}">
    
        <tr>
        
        	<td>${element.number}</td>
            <td>${element.type}</td> 
            <td>${element.departureTime}</td> 
            <td>${element.departureCity}</td>
            <td>${element.arrivalTime}</td>
            <td>${element.arrivalCity}</td>
        </tr> 
    </c:forEach>
	</table>

	<br>
	
	
	</fieldset>
	</details>
	
	</td>
	<td width=10--%> </td>
</tr>
</table>
</body>
</html>