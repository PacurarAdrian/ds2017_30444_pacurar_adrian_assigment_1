<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
form { 
    display: inline-block;
    margin-top: 0em;
}
input[type=format] {
   
    padding: 12px 20px;
    margin: 8px 0;
    display:block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}input[type=text], select{
   
 	
 	border-radius: 4px;
 	 
}
fieldset,td[bgcolor="lightgrey"]{ 
 	border-radius: 15px;
 	 
}
.modal{
	display:none;
	posiion:fixed;
	z-index:1;
	left:0;
	top:0;
	width:100%;
	height:100%;
	overflow:auto;
	background-color:rgb(0,0,0);
	background-color:rgba(0,0,0,0.4);
	
	}
.modal-content{
	background-color:#fefefe;
	margin:15% auto;
	padding: 20px;
	border: 1px solid #888;
	width: 80%;
}
.close{
	colo:#aaa;
	float: right;
	font-size:28px;
	font-weight:bold;
}
.close:hover,
.close:focus{
color:black;
text-decoration:none;
cursor:pointer;
}
</style>
<title>Airport client</title>
</head>

<body bgcolor=#D1E0E0>
<table border="0" width=100%>
<tr bgcolor=#D1D0D0><td width=10%></td><td colspan=2><font size=10 face="Viner Hand ITC">Airport</font></td><td width=10%></td></tr>

<tr>
	<td width=10% > </td>
	
	
	<td bgcolor="lightgrey" width=55%>
	<fieldset>
		<form method="GET" action="UserServlet">
			<input type="submit" name="home" value="Home"/>
		</form>
		<form id="myForm" action="UserServlet" method="POST">
			<input id="valueSearch" name="opValue" type="text" value="${search}" placeholder="Flight name or number"/>
			<input id="subSearch" type="button" onclick="submitSearch()" name="oSearch" value="Search"/>
			<input type="hidden" name="formName" value="search">
		 </form>
		 	  
				
	</fieldset>
	</td>

	<td bgcolor="lightgrey" width=25%><fieldset>

		
	<form method="get" action="LogInServlet">Logged in: ${name}   <input type="submit" value="Log out"/></form>
		
	</fieldset></td>
	
	
	<td width=10%> </td>

</tr>
<tr>	
	<td> </td>
	<td bgcolor=white colspan=2>
	<details open>
  	<summary><font size=5>Airport table</font></summary>
	<fieldset >
	<legend></legend>
	
	<br>
	


<table border="1" width="90%">
	<tr>
		<td>Number</td>
    	<td>Type</td>
    	<td>Departure Time</td>
    	<td>Departure City</td>
    	<td>Arrival Time</td>
    	<td>Arrival City</td>
        <td>Arrival City Coordinates</td>
        <td>Departure City coordinates</td>
    </tr>
    <c:forEach var="element" items="${listData}">
    
        <tr>
        
        	<td>${element.number}</td>
            <td>${element.type}</td> 
            <td>${element.departureTime}</td> 
            <td>${element.departureCity}</td>
            <td>${element.arrivalTime}</td>
            <td>${element.arrivalCity}</td>
            <td>${element.arrivalCityObj}
	            
	            <form action="UserServlet" method="POST">
	            	<input type="submit" name="getTime" value="Get local time">
	            	<input type="hidden" name="flightId" value="${element.id}">  
	            	<input type="hidden" name="city" value="arrival">
	            	<input type="hidden" name="prevSearch" value="${search}">
	            </form>
	            
            </td>
            <td>${element.departureCityObj}
	             
	         	 <form action="UserServlet" method="POST">
	            	<input type="submit" name="getTime" value="Get local time">
	            	<input type="hidden" name="flightId" value="${element.id}">  
	            	<input type="hidden" name="city" value="departure">
	            	<input type="hidden" name="prevSearch" value="${search}">
	            </form>
         	 </td>
        </tr> 
    </c:forEach>
</table>

	<br>
	
	</fieldset>
	</details>
	
	
	</td>
	<td width=10--%> </td>
</tr>
</table>

<div id="myModal" class="modal">
<div class="modal-content">
<span class="close">&times;</span>


</div>
</div>
<script>

/* var modal=document.getElementById('myModal');
var btn=document.getElementById("myBtn");
var span=document.getElementsByClassName("close")[0];
btn.onclick=function(){
	modal.style.dispay="block";
}
span.onclick=function(){
	modal.style.display="none";
	
}
window.onclick=function(event){
	if(event.target==modal)
		modal.style.display="none";
	
} */
/* var list="${listData}";
if(list.length!=1)
{
	var i=document.getElementsByTagName("button")
	for(var n in i){
		if(i[n].type="button"){
			i[n].style.visibility="hidden";
		}
	}
	
	
} */
function post(path,params,method){
	method=method||"post";
	
	var form =document.createElement("form");
	form.setAttribute("method",method);
	form.setAttribute("action",path);
	
	for(var key in params){
		if(params.hasOwnProperty(key)){
			var hiddenField=document.createElement("input");
			hiddenField.setAttribute("type","hidden");
			hiddenField.setAttribute("name",key);
			hiddenField.setAttribute("value",params[key]);
			form.appendChild(hiddenField);
		}
	}
	document.body.appendChild(form);
	form.submit();
	
}
	//function showArrival() {
		var paramOne ="${dateArrival}";
	    if(paramOne!=null && paramOne!="" && paramOne.length!=0)
	    {
	    	alert(""+paramOne);
	   		//document.getElementById("valueSearch").value="blu";
	    	//document.getElementById("myForm").submit();
	    	//post("UserServlet",{opValue: 'blu',formName: 'search'});
	    	
	    }
	    
	//}
	//function showDeparture() {
		var paramTOne ="${dateDeparture}";
	    if(paramTOne!=null && paramTOne!="" && paramTOne.length!=0)
	    {
	    	alert(""+paramTOne);
	   		//document.getElementById("valueSearch").value="blu";
	    	//document.getElementById("myForm").submit();
	    	//post("UserServlet",{opValue: 'blu',formName: 'search'});
	    	
	    }
	    
	//}
	function submitSearch(){
		//document.getElementById("subSearch").value="oSearch";
		document.getElementById("myForm").submit();
	}
	
</script>
</body>
</html>