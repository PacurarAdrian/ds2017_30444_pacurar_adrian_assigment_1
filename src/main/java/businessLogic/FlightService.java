package businessLogic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.cfg.Configuration;

import model.City;
import model.Flight;
import source.CityDAO;
import source.FlightDAO;

public class FlightService{
	
	
	private ArrayList<Flight> flights;
	private ArrayList<City> citys;
	private FlightDAO flightFile;
	private CityDAO cityFile;
	

	public FlightService()
	{
		
		this.flightFile=new FlightDAO(new Configuration().configure().buildSessionFactory());
		this.cityFile=new CityDAO(new Configuration().configure().buildSessionFactory());
		this.setFlights();
	}
	public List<Flight> getFlights() {
		return new ArrayList<Flight>(flights);
	}
	
	public List<City> getCitys() {
		return new ArrayList<City>(citys);
	}
	public String printFlights() {
		return flights.toString();
	}
	protected void setFlights(ArrayList<Flight> emps) {
		this.flights = emps;
	}
	protected void setFlights() {
		this.flights = (ArrayList<Flight>) flightFile.findFlights();
		this.citys = (ArrayList<City>) cityFile.findCitys();
		for(Flight f:flights)
		{
			for(City c:citys)
			{
				if(f.getArrivalCity().equalsIgnoreCase(c.getName()))
					f.setArrivalCityObj(c);
				if(f.getDepartureCity().equalsIgnoreCase(c.getName()))
					f.setDepartureCityObj(c);
				
			}
		}
		
	}
	protected void setCitys() {
		this.citys = (ArrayList<City>) cityFile.findCitys();
	}
	
	public ArrayList<Flight> searchFlight(String name)
	{
		ArrayList<Flight> rez=new ArrayList<Flight>();
		for(Flight e:flights)
			if(e.getType().toLowerCase().contains(name.toLowerCase()))
				rez.add(e);
		
		return rez;
	}
	
	
	public ArrayList<Flight> searchNumber(int npc)
	{
		ArrayList<Flight> rez=new ArrayList<Flight>();
		for(Flight e:flights)
			if(e.getNumber()==npc)
				rez.add(e);
		
		return rez;
	}
	public Flight searchId(int id)
	{
		
		for(Flight e:flights)
			if(e.getId()==id)
				return e;
		return null;
	}

	
	public ArrayList<Flight> deleteFlight(int npc)
	{
		if(flightFile.deleteFlight(npc)!=null)
		{
			setFlights();
			return flights;
		}else return null;
	}
	
	
	public String updateFlight(Flight Flight)
	{
		
		String str="";
		int index=-1;
		Flight copyFlight=null;
		City arr=null,dep=null;
		boolean ok1=false,ok2=false;
		for(int i=0;i<flights.size();i++)
			if(flights.get(i).getId()==Flight.getId())
			{
				
				copyFlight=flights.get(i);
				index=i;
			}
		
		
		if(!copyFlight.getArrivalCity().equals(Flight.getArrivalCity()))
		{
			
			for(City c:citys)	
			{
				if(c.getName().equals(Flight.getArrivalCity()))
				{
					ok1=true;
					arr=c;
				}
			}
			if(!ok1)
				str+=" Arrival city invalid! "+ Flight.getArrivalCity()+" ";
		}else {ok1=true;
		arr=copyFlight.getArrivalCityObj();
		}
		
		if(!copyFlight.getDepartureCity().equals(Flight.getDepartureCity()))
		{
			
			for(City c:citys)	
			{
				if(c.getName().equals(Flight.getDepartureCity()))
				{
					dep=c;
					ok2=true;
				}
			}
			if(!ok2)
				str+=" Departure city invalid! "+ Flight.getDepartureCity()+" ";
		}else {
			ok2=true;
			dep=copyFlight.getDepartureCityObj();
		}
		if(ok1&&ok2)
		{
			flightFile.updateFlight(Flight);
			flights.set(index,flightFile.findFlight(Flight.getId()));
			flights.get(index).setArrivalCityObj(arr);
			flights.get(index).setDepartureCityObj(dep);
		}
		return str;
		///this.flights = (ArrayList<model.Flight>) flightFile.findFlights();
	}
	public String insertFlight(Flight Flight)
	{
		String str="";
		int ok=0;
		for(City c:citys)	
		{
			if(c.getName().equals(Flight.getArrivalCity()))
				ok++;
		}
		if(ok<1)
			str+=" Arrival city invalid! "+ Flight.getArrivalCity()+" ";
		for(City c:citys)	
		{
			if(c.getName().equals(Flight.getDepartureCity()))
				ok++;
		}
		if(ok<2)
			str+=" Departure city invalid! "+ Flight.getDepartureCity()+" ";
		if(ok>=2)
		{
			
			flightFile.addFlight(Flight);
			setFlights();
		}
		//this.flights = (ArrayList<model.Flight>) flightFile.findFlights();
		return str;
	}
	
	public void populate()
	{
		try {
			flightFile.populate();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setFlights();
	}
	public static void main(String[] args) {
		
		FlightService temp=new FlightService();
		//System.out.println(temp.getBooks());
		
		//temp.populate();
		//temp.setFlights();
		//System.out.println(temp.searchFlight("a"));
		try {
			
			
			Flight usr=new Flight();
	
			usr.setType("boeing");
			usr.setNumber(46);
			usr.setArrivalCity("Tokyo");
			usr.setDepartureCity("Cluj");
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
			String dateInString = "21-01-2017 10:20:56";
			Date date = sdf.parse(dateInString);
			usr.setDepartureTime(date);
			usr.setId(10);
			dateInString = "21-01-2017 18:30:36";
			date = sdf.parse(dateInString);
			usr.setArrivalTime(date);
			
			System.out.println(temp.updateFlight(usr));
			//System.out.println(temp.deleteFlight("dfsrgblcvb"));
			System.out.println(temp.getFlights());
			
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}



}
