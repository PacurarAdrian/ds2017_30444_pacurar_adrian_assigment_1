package businessLogic;


import java.util.ArrayList;
import java.util.List;
import org.hibernate.cfg.Configuration;

import model.User;
import source.UserDAO;



public class UserService{
	private ArrayList<User> users;
	private UserDAO empFile;
	

	public UserService()
	{
		
		this.empFile=new UserDAO(new Configuration().configure().buildSessionFactory());;
		this.users=(ArrayList<User>) this.empFile.findUsers();
	}
	public List<User> getUsers() {
		return new ArrayList<User>(users);
	}
	public ArrayList<String> getUsernames()
	{
		ArrayList<String> names=new ArrayList<String>();
		for(User u:users)
			names.add(u.getUsername());
		return names;
	}
	public ArrayList<String> getPasswords()
	{
		ArrayList<String> passwords=new ArrayList<String>();
		for(User u:users)
			passwords.add(u.getPassword());
		return passwords;

	}
	public String printUsers() {
		return users.toString();
	}
	protected void setUsers(ArrayList<User> emps) {
		this.users = emps;
	}
	protected void setUsers() {
		this.users = (ArrayList<User>) empFile.findUsers();
	}
	
	
	public ArrayList<User> searchUser(String name)
	{
		ArrayList<User> rez=new ArrayList<User>();
		for(User e:users)
			if(e.getName().contains(name))
				rez.add(e);
		
		return rez;
	}
	public ArrayList<User> searchName(String name)
	{
		ArrayList<User> rez=new ArrayList<User>();
		for(User e:users)
			if(e.getName().contains((name)))
				rez.add(e);
		
		return rez;
	}
	public ArrayList<User> searchPassword(String name)
	{
		ArrayList<User> rez=new ArrayList<User>();
		for(User e:users)
			if(e.getPassword().equals((name)))
				rez.add(e);
		
		return rez;
	}
	public User searchNpc(String npc)
	{
		
		for(User e:users)
			if(e.getNpc().equals(npc))
				return e;
		
		return null;
	}
	public boolean existsAdmin()
	{
		for(User e:users)
			if(e.getAdmin())
				return true;
		
		return false;
	}
	public User getAdmin()
	{
		for(User e:users)
			if(e.getAdmin())
				return e;
		
		return null;
	}
	public Boolean deleteUser(String npc)
	{
		if(empFile.deleteUser(npc)!=null)
		{
			this.users = (ArrayList<User>) empFile.findUsers();
			return true;
		}else return false;
	}
	public ArrayList<User> deleteUser(int id)
	{
		empFile.deleteUser(id);
		
			this.users = (ArrayList<User>) empFile.findUsers();
		return users;
	}
	
	
	public void updateUser(User User)
	{
		empFile.updateUser(User);
		this.users = (ArrayList<model.User>) empFile.findUsers();
	}
	public void insertUser(User User)
	{
		empFile.addUser(User);
		this.users = (ArrayList<model.User>) empFile.findUsers();
	}
	
	public void populate()
	{
		empFile.populate();
		this.users = (ArrayList<User>) empFile.findUsers();
	}
	public static void main(String[] args) {
		
		UserService temp=new UserService();
		
		
		//temp.populate();
		//System.out.println(temp.searchUser("a"));
		try {
			//User usr=temp.searchNpc("1234567890");
			//usr.setAdmin(true);
			//temp.updateUser(usr);
			System.out.println(temp.getUsers());
			//System.out.println(temp.deleteUser("dfsrgblcvb"));
			
		} catch (IllegalArgumentException e) {
			
			e.printStackTrace();
		}
		
	}
}