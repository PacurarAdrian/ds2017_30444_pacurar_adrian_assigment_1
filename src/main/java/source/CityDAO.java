package source;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.City;

public class CityDAO {
	private static final Log LOGGER = LogFactory.getLog(CityDAO.class);

	private SessionFactory factory;

	public CityDAO(SessionFactory factory) {
		this.factory = factory;
	}

	public City addCity(City City) {
		int CityId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			CityId = (Integer) session.save(City);
			City.setId(CityId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return City;
	}

	@SuppressWarnings("unchecked")
	public List<City> findCitys() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> Citys = null;
		try {
			tx = session.beginTransaction();
			Citys = session.createQuery("FROM City").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Citys;
	}

	@SuppressWarnings("unchecked")
	public City findCity(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<City> Citys = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE id = :id");
			query.setParameter("id", id);
			Citys = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Citys != null && !Citys.isEmpty() ? Citys.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public City deleteCity(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<City> Citys = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE id = :id");
			query.setParameter("id", id);
			Citys = query.list();
			query = session.createQuery("delete City WHERE id = :id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Expensive products was removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Citys != null && !Citys.isEmpty() ? Citys.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public City deleteCity(String name) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<City> Citys = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE Name = :name");
			query.setParameter("name", name);
			Citys = query.list();
			query = session.createQuery("delete City WHERE Name = :name");
			query.setParameter("pnc", name);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("City "+Citys.get(0)+" was removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Citys != null && !Citys.isEmpty() ? Citys.get(0) : null;
	}
public void updateCity(City city) {

	Session session = factory.openSession();
	Transaction tx = null;
	try {
		tx = session.beginTransaction();
		session.saveOrUpdate(city);
		tx.commit();
	} catch (HibernateException e) {
		if (tx != null) {
			tx.rollback();
		}
		LOGGER.error("", e);
	} finally {
		session.close();
	}
	
	
}
public void populate() {
		
		City city = new City();
		city.setName("Cluj");
		
		city.setLatitude((float)46.771210);
		city.setLongitude((float)23.623635);
		this.addCity(city);
		
		city=new City();
		city.setName("London");
		city.setLatitude((float)51.507351);
		city.setLongitude((float)23.623635);
		addCity(city);
		
		city=new City();
		city.setName("Tokyo");
		city.setLatitude((float)35.709026);
		city.setLongitude((float)139.731992);
		addCity(city);
		
		
		city=new City();
		city.setName("Moscow");
		city.setLatitude((float)55.755826);
		city.setLongitude((float)37.617300);
		
		addCity(city);
		
	}

	
	public static void main(String[] args) {
		CityDAO cityDao = new CityDAO(new Configuration().configure().buildSessionFactory());
		City city=new City();
		city.setName("Moscow");
		city.setLatitude((float)55.755826);
		city.setLongitude((float)37.617300);
		
		cityDao.populate();
		System.out.println(city.getId());
	}



}
