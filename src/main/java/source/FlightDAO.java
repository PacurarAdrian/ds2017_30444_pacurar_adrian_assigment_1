package source;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import model.Flight;

public class FlightDAO {

	private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);

	private SessionFactory factory;

	public FlightDAO(SessionFactory factory) {
		this.factory = factory;
	}

	public Flight addFlight(Flight Flight) throws HibernateException {
		int FlightId = -1;
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			FlightId = (Integer) session.save(Flight);
			Flight.setId(FlightId);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
			throw new HibernateException("could not insert flight to database");
		} finally {
			session.close();
		}
		return Flight;
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findFlights() {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> Flights = null;
		try {
			tx = session.beginTransaction();
			Flights = session.createQuery("FROM Flight").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Flights;
	}

	@SuppressWarnings("unchecked")
	public Flight findFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		List<Flight> Flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			Flights = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Flights != null && !Flights.isEmpty() ? Flights.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public Flight deleteFlight(int id) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<Flight> Flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE id = :id");
			query.setParameter("id", id);
			Flights = query.list();
			query = session.createQuery("delete Flight WHERE id = :id");
			query.setParameter("id", id);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Flight was removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return Flights != null && !Flights.isEmpty() ? Flights.get(0) : null;
	}
	@SuppressWarnings("unchecked")
	public Flight deleteFlight(String type) {
		Session session = factory.openSession();
		Transaction tx = null;
		int result=0;
		List<Flight> flights = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE Airplane type = :type");
			query.setParameter("type", type);
			flights = query.list();
			query = session.createQuery("delete Flight WHERE Airplane type = :type");
			query.setParameter("type", type);
			result = query.executeUpdate();
			if (result > 0) {
			    System.out.println("Flights "+flights+" were removed");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
			LOGGER.error("", e);
		} finally {
			session.close();
		}
		return flights != null && !flights.isEmpty() ? flights.get(0) : null;
	}
public void updateFlight(Flight flight) {

	Session session = factory.openSession();
	Transaction tx = null;
	try {
		tx = session.beginTransaction();
		session.saveOrUpdate(flight);
		tx.commit();
	} catch (HibernateException e) {
		if (tx != null) {
			tx.rollback();
		}
		LOGGER.error("", e);
		throw new HibernateException("could not insert flight to database");
	} finally {
		session.close();
	}
	
	
}
public void populate() throws ParseException {
		
		Flight Flight = new Flight();
		
		Flight.setType("boeing");
		Flight.setNumber(46);
		Flight.setDepartureCity("Cluj");
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		String dateInString = "21-01-2017 10:20:56";
		Date date = sdf.parse(dateInString);
		Flight.setDepartureTime(date);
		Flight.setArrivalCity("Tokyo");
		dateInString = "21-01-2017 18:30:36";
		date = sdf.parse(dateInString);
		Flight.setArrivalTime(date);
		
		this.addFlight(Flight);
		
		Flight.setType("Bluej");
		Flight.setNumber(146);
		Flight.setDepartureCity("Moscow");
		sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		dateInString = "21-12-2016 10:20:56";
		date = sdf.parse(dateInString);
		Flight.setDepartureTime(date);
		Flight.setArrivalCity("London");
		dateInString = "21-12-2016 14:30:36";
		date = sdf.parse(dateInString);
		Flight.setArrivalTime(date);
		addFlight(Flight);
		
		Flight=new Flight();
		Flight.setType("Albatros");
		Flight.setNumber(6);
		Flight.setDepartureCity("Tokyo");
		sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		dateInString = "01-01-2017 21:20:56";
		date = sdf.parse(dateInString);
		Flight.setDepartureTime(date);
		Flight.setArrivalCity("Moscow");
		dateInString = "02-01-2017 01:30:36";
		date = sdf.parse(dateInString);
		Flight.setArrivalTime(date);
		addFlight(Flight);
		
		
		Flight=new Flight();
		Flight.setType("Eagle");
		Flight.setNumber(10);
		Flight.setDepartureCity("London");
		sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		dateInString = "02-02-2017 11:20:56";
		date = sdf.parse(dateInString);
		Flight.setDepartureTime(date);
		Flight.setArrivalCity("Cluj");
		dateInString = "02-02-2017 22:30:36";
		date = sdf.parse(dateInString);
		Flight.setArrivalTime(date);
		
		addFlight(Flight);
		
	}

	
	public static void main(String[] args) throws ParseException {
		FlightDAO FlightDao = new FlightDAO(new Configuration().configure().buildSessionFactory());
		
		
		FlightDao.populate();
		
	}



}
