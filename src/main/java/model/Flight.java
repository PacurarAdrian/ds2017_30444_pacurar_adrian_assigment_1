package model;

import java.util.Date;

public class Flight {
	private int id;
	private String type;
	private Date departureTime;
	private Date arrivalTime;
	private String departureCity;
	private String arrivalCity;
	private int number;
	private City arrivalCityObj;
	private City departureCityObj;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public String getDepartureCity() {
		return departureCity;
	}
	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	public String getArrivalCity() {
		return arrivalCity;
	}
	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public City getDepartureCityObj() {
		return departureCityObj;
	}
	public void setDepartureCityObj(City departureCityObj) {
		this.departureCityObj = departureCityObj;
	}
	public City getArrivalCityObj() {
		return arrivalCityObj;
	}
	public void setArrivalCityObj(City arrivalCityObj) {
		this.arrivalCityObj = arrivalCityObj;
	}
	
	
	
}
