package model;

public class City {
	private int id;
	private String name;
	private float latitude;
	private float longitude;
	
	
	public String toString()
	{
		return "City "+name+" has latitude: "+latitude+" longitude "+longitude+"\n";
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
