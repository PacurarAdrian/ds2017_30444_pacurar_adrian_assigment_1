/**
 * 
 */
package model;

/**
 * @author Adi
 *
 */

public class User {

	private String name;
	private String npc;
	private String username;
	private String password;
	private Boolean admin;	
	private int id;
	
	public User(String name,String npc,String username,String password,Boolean admin)
	{
		this.name=name;
		this.npc=npc;
		this.username=username;
		this.password=password;
		this.admin=admin;
	}
	public User()
	{
		
	}
	public String toString()
	{
		return id+": Employee with NPC "+npc+" is named "+name+", has username "+username+", password "+password;
	}
	
	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getNpc() {
		return npc;
	}




	public void setNpc(String npc) {
		this.npc = npc;
	}




	public String getUsername() {
		return username;
	}




	public void setUsername(String username) {
		this.username = username;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public Boolean getAdmin() {
		return admin;
	}




	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}

	

	

}
