package prezentation;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import businessLogic.FlightService;
import model.City;
import model.Flight;
/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightService module;
///	private UserService users;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        System.out.println("constructor---->in");
       // users=new UserService();
        module=new FlightService();
        System.out.println("constructor---->out");
        
    }
    Date getLocalTime(String url)
	{
		Date date=null;
		try {	
	         
	         DocumentBuilderFactory dbFactory 
	            = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(new URL(url).openStream());
	         doc.getDocumentElement().normalize();
	         //System.out.println("\nRoot element :" + doc.getDocumentElement().getNodeName());
	         NodeList nList = doc.getElementsByTagName("timezone");
	         //System.out.println("----------------------------");
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	        	
	            Node nNode = nList.item(temp);
	            //System.out.println("\nCurrent Element :" + nNode.getNodeName());
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) nNode;
	               
	              
	               
	               //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm:ss Z");
	               SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy HH:mm:ss");
	       		String dateInString =  eElement.getElementsByTagName("localtime").item(0).getTextContent();
	       		date = sdf.parse(dateInString);
	       		 //local=new Date(date.getTime()+TimeZone.getDefault().getOffset(new Date().getTime()));
	             System.out.println("Date String: "+dateInString );
	                 
	               
	             }
	           
	         }
	        
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
		 return date;
		
	}
    private void search(String temp,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	if(temp==""|| temp==null)
	       {
	    	
	    	   try{
	    		   //request.setAttribute("table", module.getTable(module.getFlights()));
	    		   request.setAttribute("listData",module.getFlights());
	    	   }
				catch(Exception e){
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
					//throw new StoreException("User search could not be displayed");
				}
			    	
	       }else{
	    	   System.out.println("intra in search cu string: "+temp);
	    	   String regex=temp;
	    	   Pattern p = Pattern.compile("[0-9]*");
	  	    	Matcher m = p.matcher(regex);
	  	    	if (!m.matches())
	  	    	{
	  	    		try {
	  	    			List<Flight> values=module.searchFlight(temp);
	  	    			//request.setAttribute("table", module.getTable(values));
	  	    			request.setAttribute("listData",values);
	  	    			/*if(values.size()==1)
	  	    			{
	  	    				City c=values.get(0).getArrivalCityObj();
	  	    				Date d=getLocalTime("http://new.earthtools.org/timezone/"+c.getLatitude()+"/"+c.getLongitude());
	  	    				System.out.println("Date in arrival city: "+d);
	  	    				request.setAttribute("dateArrival","Date Arrival: "+d);
	  	    				c=values.get(0).getDepartureCityObj();
	  	    				 d=getLocalTime("http://new.earthtools.org/timezone/"+c.getLatitude()+"/"+c.getLongitude());
	  	    				System.out.println("Date in departure city: "+d);
	  	    				request.setAttribute("dateDeparture","Date Departure: "+d);
	  	    			
	  	    			}*/
					} catch (Exception e) {
						//throw new StoreException("User search could not be displayed");
						e.printStackTrace();
						request.setAttribute("exception",e.getMessage());
						 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
					     dispatcher.include(request,response);
					}
	  	    	}else{
	    	  
					try {
						
						//request.setAttribute("table", module.getTable(module.searchNumber(Integer.parseInt(temp))));
						List<Flight> values=module.searchNumber(Integer.parseInt(temp));
						request.setAttribute("listData",values);
						/*if(values.size()==1)
	  	    			{
							City c=values.get(0).getArrivalCityObj();
	  	    				Date d=getLocalTime("http://new.earthtools.org/timezone/"+c.getLatitude()+"/"+c.getLongitude());
	  	    				System.out.println("Date in arrival city: "+d);
	  	    				request.setAttribute("dateArrival","Date Arrival: "+d);
	  	    				c=values.get(0).getDepartureCityObj();
	  	    				 d=getLocalTime("http://new.earthtools.org/timezone/"+c.getLatitude()+"/"+c.getLongitude());
	  	    				System.out.println("Date in departure city: "+d);
	  	    				request.setAttribute("dateDeparture","Date Departure: "+d);
	  	    			}*/
					} catch (Exception e) {
						
						
						e.printStackTrace();
						request.setAttribute("exception",e.getMessage());
						 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
					     dispatcher.include(request,response);
					}
	  	    	}
		    	   
	       }
		request.setAttribute("search", temp);
		 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
	    	dispatcher.include(request,response);
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("intra in doGet");
		HttpSession session = request.getSession(true);
		String temp=request.getParameter("getArrival");
		System.out.println("prev search :"+temp);
    	if(session.getAttribute("name")!=null)
    	{
			if(request.getParameter("home")!=null)
			{
				try {
					request.setAttribute("listData",module.getFlights());
					//request.setAttribute("table",module.getTable(module.getFlights()));
					//System.out.println(module.getTable(module.getUsers()));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
	        	dispatcher.forward(request,response);
				
			}else
			if(request.getParameter("getArrival")!=null)
			{
				search(temp, request, response);
				
			}
			
			else{
				
				try {
					//request.setAttribute("table",module.getTable(module.getFlights()));
					request.setAttribute("listData",module.getFlights());
					//System.out.println(module.getTable(module.getUsers()));
				} catch (IllegalArgumentException e) {
				
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
	        	dispatcher.forward(request,response);
			}
    	}else
    	{
    		
    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
    	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("intra in doPost");
		if(request.getParameter("formName")!=null)
		{
			System.out.println("intra in search");
			 String temp = request.getParameter("opValue");
			 search(temp, request, response);
			
		}else 
		if(request.getParameter("getTime")!=null)
		{
			String temp = request.getParameter("prevSearch");
			
			int id=Integer.parseInt(request.getParameter("flightId"));
			String direction=request.getParameter("city");
			Flight values=module.searchId(id);
			City c;
			Date d;
			switch(direction)
			{
				case "arrival":
				
				c=values.getArrivalCityObj();
  				d=getLocalTime("http://new.earthtools.org/timezone/"+c.getLatitude()+"/"+c.getLongitude());
  				System.out.println("Date in arrival city: "+d);
  				request.setAttribute("dateArrival","Date Arrival: "+d);
  				break;
				case "departure":
				c=values.getDepartureCityObj();
					 d=getLocalTime("http://new.earthtools.org/timezone/"+c.getLatitude()+"/"+c.getLongitude());
					System.out.println("Date in departure city: "+d);
					request.setAttribute("dateDeparture","Date Departure: "+d);
					break;
			}
			search(temp, request, response);
			
		}
		else 
		{
			
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
        	dispatcher.forward(request,response);
		}
    	
		//doGet(request,response);
		
		
	}

}
