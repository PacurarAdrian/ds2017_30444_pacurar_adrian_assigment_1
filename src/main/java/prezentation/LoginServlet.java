package prezentation;

import java.io.IOException;
import java.util.ArrayList;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import businessLogic.FlightService;
import businessLogic.UserService;
import model.User;


/**
 * Servlet implementation class LoginServlet
 */
//@WebServlet(
//        description = "Login Servlet", 
//        urlPatterns = { "/" }, 
//        initParams = { 
//                @WebInitParam(name = "user", value = "Pankaj"), 
//                @WebInitParam(name = "password", value = "journaldev")
//        })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */ 
    public LoginServlet() {
        super();
		
    }
    
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		if(session.getAttribute("name")!=null)
    	{
			session.setAttribute("name", null);
			session.setAttribute("id", null);
			request.setAttribute("message", "You have been logged out successfully");
			
    	}
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
        rd.include(request, response);
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		 //get request parameters for userID and password
        String user = request.getParameter("user");
        String pwd = request.getParameter("pwd");
    	String userID = null ;
        String password = null ;
        ArrayList<String> names = null;
        ArrayList<String> passwords = null;
		
        UserService userService=new UserService();
		User rez = null;
		try {
			rez = userService.getAdmin();
			System.out.println(rez);
			userID = rez.getUsername();
		    password = rez.getPassword();
		    names = userService.getUsernames();
		    passwords=userService.getPasswords();
		   
		 
	        //logging example
	        log("User="+user+"::password="+pwd);
	         
	        if(userID.equalsIgnoreCase(user) && password.equals(pwd)){
	        	
	        	HttpSession session = request.getSession(true);
	        	session.setAttribute("name", rez.getName());
	        	session.setAttribute("id", rez.getId());
	        	
				FlightService flights=new FlightService();            	
            	
            	request.setAttribute("userData", userService.getUsers());
				request.setAttribute("flightData", flights.getFlights());
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminHome.jsp");
	        	dispatcher.forward(request,response);
	           
	        }else{
	        	if(names.contains(user) && passwords.contains(pwd))
	        	{
	        		HttpSession session = request.getSession(true);
	        		FlightService flights=new FlightService();
	            	session.setAttribute("name", userService.searchPassword(pwd).get(0).getName());
	            	
	            	request.setAttribute("listData", flights.getFlights());
					
	            	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/userHome.jsp");
	            	dispatcher.include(request,response);
	        	}else
	        	{
		            RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
		            

		            request.setAttribute("message", "Either username or password is wrong!");
		            rd.include(request, response);
		        	}
	             
	        }
		} catch (Exception e) {
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/login.jsp");
            request.setAttribute("message", "Problem occured while trying to connect to database!");
            rd.include(request, response);
			e.printStackTrace();
		}
        
	}

}
