package prezentation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import businessLogic.FlightService;
import businessLogic.UserService;
import model.Flight;
import model.User;
/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FlightService flights;
	private UserService users;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        System.out.println("constructor---->in");
        users=new UserService();
        flights=new FlightService();
        System.out.println("constructor---->out");
        
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		HttpSession session = request.getSession(true);
    	if(session.getAttribute("id")!=null&&Integer.valueOf(users.getAdmin().getId())==session.getAttribute("id"))
    	{
			if(request.getParameter("home")!=null)
			{
				try {
					
					request.setAttribute("userData", users.getUsers());
					request.setAttribute("flightData", flights.getFlights());
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
					
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminHome.jsp");
	        	dispatcher.forward(request,response);
				
			}else
			
			if(request.getParameter("edit")!=null)
			{
				try {
					List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
					request.setAttribute("adminData",list);
					request.setAttribute("userData",users.getUsers());
					//request.setAttribute("tableName","users");
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
	        	dispatcher.forward(request,response);
			}else
			if(request.getParameter("editFlights")!=null)
			{
				try {
					
					request.setAttribute("flightData", flights.getFlights());
					request.setAttribute("cityData",flights.getCitys());
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditFlight.jsp");
	        	dispatcher.forward(request,response);	
			}
			else
			{
				try {
					
					request.setAttribute("userData", users.getUsers());
					request.setAttribute("flightData", flights.getFlights());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
				}
				
	        	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminHome.jsp");
	        	dispatcher.forward(request,response);
			}
    	}else
    	{
    		
    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
    	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */ 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
    	if(session.getAttribute("id")!=null&&Integer.valueOf(users.getAdmin().getId())==session.getAttribute("id"))
    	{
		if(request.getParameter("oSearchUser")!=null)
		{
			 String temp = request.getParameter("opValue");
			 
				 if(temp==""|| temp==null)
			       {
			    	
			    	   try{
			    		   List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
							request.setAttribute("adminData",list);
							request.setAttribute("userData",users.getUsers());
							
			    		  
			    	   }
						catch(Exception e){
							
							e.printStackTrace();
							request.setAttribute("exception",e.getMessage());
							 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
						     dispatcher.include(request,response);
						}
					    	
			       }else{
			    	   String regex=temp;
			    	   Pattern p = Pattern.compile("[0-9]*");
			  	    	Matcher m = p.matcher(regex);
			  	    	if (!m.matches())
			  	    	{
			  	    		try {
			  	    			List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
								request.setAttribute("adminData",list);
								request.setAttribute("userData",users.searchName(temp));
							} catch (Exception e) {
								
								e.printStackTrace();
								request.setAttribute("exception",e.getMessage());
								 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
							     dispatcher.include(request,response);
							}
			  	    	}else{
			    	  
							try {
								List<User> npc=new ArrayList<User>();
								npc.add(users.searchNpc(temp));
								List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
								request.setAttribute("adminData",list);
								request.setAttribute("userData",npc);
							
							} catch (Exception e) {
								
								
								e.printStackTrace();
								request.setAttribute("exception",e.getMessage());
								 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
							     dispatcher.include(request,response);
							}
			  	    	}
				    	   
			       }
			 
			// request.setAttribute("tableName",request.getParameter("tableName"));
			 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
			 dispatcher.include(request,response);
		}else if(request.getParameter("oSearchFlight")!=null)
		{
			String temp = request.getParameter("opValue");
			 if(temp==""|| temp==null)
		       {
			    	
		    	   try{
		    		  // request.setAttribute("table", flights.getTable(flights.getFlights()));
		    		   request.setAttribute("flightData", flights.getFlights());
						request.setAttribute("cityData",flights.getCitys());
		    	   }
					catch(Exception e){
						//throw new StoreException("User search could not be displayed");
						e.printStackTrace();
						request.setAttribute("exception",e.getMessage());
						 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
					     dispatcher.include(request,response);
					}
				    	
		       }else{
		    	   String regex=temp;
		    	   Pattern p = Pattern.compile("[0-9]*");
		  	    	Matcher m = p.matcher(regex);
		  	    	if (!m.matches())
		  	    	{
		  	    		try {
		  	    			//request.setAttribute("table",flights.getTable(flights.searchFlight(temp)));
		  	    			request.setAttribute("flightData", flights.searchFlight(temp));
							request.setAttribute("cityData",flights.getCitys());
						} catch (Exception e) {
							//throw new StoreException("User search could not be displayed");
							e.printStackTrace();
							request.setAttribute("exception",e.getMessage());
							 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
						     dispatcher.include(request,response);
						}
		  	    	}else{
		    	  
						try {
							
							//request.setAttribute("table", flights.getTable(flights.searchNumber(Integer.parseInt(temp))));
							request.setAttribute("flightData", flights.searchNumber(Integer.parseInt(temp)));
							request.setAttribute("cityData",flights.getCitys());
						} catch (Exception e) {
							
							
							e.printStackTrace();
							request.setAttribute("exception",e.getMessage());
							 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
						     dispatcher.include(request,response);
						}
		  	    	}
			    	   
		       }
			 request.setAttribute("tableName",request.getParameter("tableName"));
			 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditFlight.jsp");
		    	dispatcher.include(request,response);
			
		}else 
			
		if(request.getParameter("oDelete")!=null)
		{
			try{
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de sters:"+id);
				request.setAttribute("cityData",flights.getCitys());
				request.setAttribute("flightData",flights.deleteFlight(id));
				request.setAttribute("tableName",request.getParameter("tableName"));
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditFlight.jsp");
				dispatcher.forward(request,response);
			}catch(Exception e)
			{
				e.printStackTrace();
				request.setAttribute("exception",e.getMessage());
				 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
			     dispatcher.include(request,response);
			}
			
			
		}else
			if(request.getParameter("oDeleteUser")!=null)
			{
				try{
					int id=Integer.parseInt(request.getParameter("elemId"));
					System.out.println("id de sters:"+id);
					List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
					request.setAttribute("adminData",list);
					request.setAttribute("userData",users.deleteUser(id));
					request.setAttribute("tableName",request.getParameter("tableName"));
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
					dispatcher.forward(request,response);
				}catch(Exception e)
				{
					e.printStackTrace();
					request.setAttribute("exception",e.getMessage());
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
				}
				
				
			}else
		if(request.getParameter("oEdit")!=null)
		{
			try{
				int id=Integer.parseInt(request.getParameter("elemId"));
				System.out.println("id de update:"+id);
				Flight f=new Flight();
				f.setId(id);
				f.setArrivalCity(request.getParameter("arrivalCity"));
				f.setDepartureCity(request.getParameter("departureCity"));
				f.setNumber(Integer.parseInt(request.getParameter("number")));
				f.setType(request.getParameter("type"));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm");
				String dateInString = request.getParameter("departureTime");
				dateInString=dateInString.replace('T',' ');
				Date date;
				
					date = sdf.parse(dateInString);
					f.setDepartureTime(date);
					dateInString = request.getParameter("arrivalTime");
					dateInString=dateInString.replace('T',' ');
					date = sdf.parse(dateInString);
					f.setArrivalTime(date);
				
					String exc;
					if((exc=flights.updateFlight(f))!="")
						throw new Exception(exc);
				
				request.setAttribute("cityData",flights.getCitys());
				request.setAttribute("flightData",flights.getFlights());
				request.setAttribute("tableName",request.getParameter("tableName"));
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditFlight.jsp");
				dispatcher.forward(request,response);
			}catch(Exception e)
			{
				e.printStackTrace();
				request.setAttribute("exception","Invalid input detected:"+e.getMessage()+"please reenter data");
				 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
			     dispatcher.include(request,response);
			}
			
			
		}else
			if(request.getParameter("oEditUser")!=null)
			{
				try{
					int id=Integer.parseInt(request.getParameter("elemId"));
					System.out.println("id de update:"+id);
					User f=new User();
					f.setId(id);
					f.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
					f.setName(request.getParameter("name"));
					f.setNpc(request.getParameter("npc"));
					f.setPassword(request.getParameter("password"));
					f.setUsername(request.getParameter("username"));
					
					users.updateUser(f);
					
					List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
					request.setAttribute("adminData",list);
					request.setAttribute("userData",users.getUsers());
					request.setAttribute("tableName",request.getParameter("tableName"));
					RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
					dispatcher.forward(request,response);
				}catch(Exception e)
				{
					e.printStackTrace();
					request.setAttribute("exception","Invalid input detected:"+e.getMessage()+"please reenter data");
					 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
				     dispatcher.include(request,response);
				}
				
				
			}else
		if(request.getParameter("oInsert")!=null)
		{
			try {
			Flight f=new Flight();
			f.setArrivalCity(request.getParameter("arrivalCity"));
			f.setDepartureCity(request.getParameter("departureCity"));
			f.setNumber(Integer.parseInt(request.getParameter("number")));
			f.setType(request.getParameter("type"));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm");
			String dateInString = request.getParameter("departureTime");
			dateInString=dateInString.replace('T',' ');
			Date date;
			
				date = sdf.parse(dateInString);
				f.setDepartureTime(date);
				dateInString = request.getParameter("arrivalTime");
				dateInString=dateInString.replace('T',' ');
				date = sdf.parse(dateInString);
				f.setArrivalTime(date);
			
				String exc;
				if((exc=flights.insertFlight(f))!="")
					throw new Exception(exc);
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("exception","Invalid input detected:"+e.getMessage()+"please reenter data");
				 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
			     dispatcher.forward(request,response);
			}
			request.setAttribute("flightData", flights.getFlights());
			request.setAttribute("cityData",flights.getCitys());
			request.setAttribute("tableName",request.getParameter("tableName"));
		    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditFlight.jsp");
		    dispatcher.include(request,response);
		}else 
			if(request.getParameter("oInsertUser")!=null)
		{
			try {
				
				
				User f=new User();
				
				f.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
				f.setName(request.getParameter("name"));
				f.setNpc(request.getParameter("npc"));
				f.setPassword(request.getParameter("password"));
				f.setUsername(request.getParameter("username"));
				
				users.insertUser(f);
				
				List<Boolean> list=Arrays.asList(new Boolean[]{true,false});
				request.setAttribute("adminData",list);
				request.setAttribute("userData",users.getUsers());
				request.setAttribute("tableName",request.getParameter("tableName"));
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/adminEditUser.jsp");
				dispatcher.include(request,response);
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("exception","Invalid input detected:"+e.getMessage()+"please reenter data");
				 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ExceptionPage.jsp");
			     dispatcher.forward(request,response);
			}
			
		}
		
    	}else
    	{
    		
    		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/accessDenied.jsp");
        	dispatcher.forward(request,response);
    	}
		
	}

}
